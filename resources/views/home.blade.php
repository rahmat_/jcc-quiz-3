@extends('layout.master')
@section('judul')
    Halaman Utama
@endsection
@section('content')
    <h2>Media Online</h2>
    <h4>Sosial Media Developer</h4>
    <p>Belajar dan berbagi agar hidup lebih baik</p>
    <h4>Benefit Join di Media Online</h4>
    <ul>
        <li>Mendapatkan motivasi dari sesama para Developer</li>
        <li>Sharing knowlage</li>
        <li>Dibuat oleh calon web developer terbaik</li>
    </ul>

    <h4>Cara Bergabung ke Media Online</h4>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
        <li>Selesai</li>
    </ol>
@endsection