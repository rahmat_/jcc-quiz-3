<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function pendataan()
    {
        return view('halaman.pendataan');
    }

    public function welcome(Request $request)
    {
        //dd($request->all());
        $first_name = $request['first_name'];
        $last_name = $request['last_name'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $language = $request['language'];
        $bio = $request['bio'];

        return view('halaman.welcome', compact('first_name', 'last_name', 'gender', 'nationality', 'language', 'bio'));
    }
}
